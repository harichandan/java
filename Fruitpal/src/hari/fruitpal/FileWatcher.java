package hari.fruitpal;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import redis.clients.jedis.Jedis;

public class FileWatcher {
	Jedis jedis;
	Path rootDir;
	WatchService watchService;

	private void startRedisClient() {
		/*
		 * Create a redis-client
		 */
		this.jedis = new Jedis("localhost", 6379);
	}

	FileWatcher(String path) {
		rootDir = Paths.get(path);
		/*
		 * Create a watch service to watch the data directory for create and
		 * modify events
		 */
		try {
			watchService = FileSystems.getDefault().newWatchService();
		} catch (IOException e) {
			System.err.println("Caught IOException: " + e.getMessage());
		}
		try {
			rootDir.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
		} catch (IOException e) {
			System.err.println("Caught IOException: " + e.getMessage());
		}

		startRedisClient();
	}

	public void startFileWatcher() throws FileNotFoundException, IOException, InterruptedException {

		while (true) {
			/*
			 * Wait for an event to be added to the event queue and retrieve it
			 */
			WatchKey queuedKey = watchService.take();
			java.util.List<WatchEvent<?>> events = queuedKey.pollEvents();

			for (WatchEvent<?> event : events) {
				/*
				 * For each event, get the relative path from the project
				 * directory
				 */
				Path relativeFilePath = Paths.get(rootDir.toString(), event.context().toString());

				/*
				 * If the file is .txt file, i.e., file containing the 3rd party
				 * data, flush all the existing keys from redis, and create new
				 * keys from the new data
				 */
				if (relativeFilePath.toString().endsWith(".txt")) {
					jedis.flushAll();

					try (BufferedReader br = new BufferedReader(new FileReader(relativeFilePath.toString()))) {
						for (String line; (line = br.readLine()) != null;) {

							/*
							 * Example input: MANGO MX 32 1.24 Split the input
							 * into an array of strings
							 */
							String[] fruitData = line.split(" ");

							/*
							 * Insert a set with key set to the fruit name, and
							 * the rest of the data as colon separated string.
							 * 
							 * For example, If lines from the file are: MANGO MX
							 * 32 1.24 MANGO BR 20 1.42 Then redis will contain:
							 * "MANGO": {"MX:32:1.24","BR:20:1.42"}
							 */
							jedis.sadd(fruitData[0], fruitData[1] + ":" + fruitData[2] + ":" + fruitData[3]);
						}
					}
				}
			}

			boolean valid = queuedKey.reset();
			if (!valid) {
				break;
			}
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		FileWatcher watcher = new FileWatcher("data/");
		watcher.startFileWatcher();
	}
}
