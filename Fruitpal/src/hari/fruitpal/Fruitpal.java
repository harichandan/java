package hari.fruitpal;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Set;

import redis.clients.jedis.Jedis;

public class Fruitpal {
	private static final int DEFAULT_CAPACITY = 25;
	private PriorityQueue<DataRow> maxHeap;
	private Jedis jedis;
	private double tradeVolume;
	private double pricePerTon;
	private String fruit;

	private class DataRow {
		private String country;
		private double fixedOverhead;
		private double varOverhead;
		private double totalCost;

		DataRow(String country, String fixedOverhead, String varOverhead) {
			this.country = country;
			this.fixedOverhead = Double.parseDouble(fixedOverhead);
			this.varOverhead = Double.parseDouble(varOverhead);
		}

		private double round(double e) {
			return Math.round(e * 100) / 100.0;
		}

		public double getFixedOverhead() {
			return fixedOverhead;
		}

		public double getVarOverhead() {
			return varOverhead;
		}

		public String getCountry() {
			return country;
		}

		public void setTotalCost(double tradeVolume, double ppt) {
			totalCost = round(tradeVolume * (ppt + varOverhead) + fixedOverhead);
		}

		public double getTotalCost() {
			return totalCost;
		}
	}

	private void populatePriorityQueue() {
		/*
		 * For each fruit line in the data file: MANGO MX 32 1.24 MANGO BR 20
		 * 1.42 Then redis will contain the data in the following format:
		 * "MANGO": {"MX:32:1.24","BR:20:1.42"}
		 *
		 * Iterate over the values, creating a DataRow object and inserting into
		 * the PriorityQueue
		 */
		Set<String> set = jedis.smembers(fruit);
		for (String s : set) {
			String[] fruitData = s.split(":");
			DataRow row = new DataRow(fruitData[0], fruitData[1], fruitData[2]);
			row.setTotalCost(tradeVolume, pricePerTon);
			maxHeap.add(row);
		}
	}

	private void validateInputArgument(double arg) throws IllegalArgumentException {
		if (arg < 0)
			throw new IllegalArgumentException();
	}

	Fruitpal(String[] args) {
		fruit = args[0].toUpperCase();
		try {
			tradeVolume = Double.parseDouble(args[1]);
			validateInputArgument(tradeVolume);
		} catch (NumberFormatException e) {
			System.err.println("NumberFormatException: " + e.getMessage());
			System.exit(2);
		}

		try {
			pricePerTon = Double.parseDouble(args[2]);
			validateInputArgument(pricePerTon);
		} catch (NumberFormatException e) {
			System.err.println("NumberFormatException: " + e.getMessage());
			System.exit(2);
		}

		/*
		 * Create a PriorityQueue with the order of the DataRow objects in
		 * decreasing order of total cost
		 */
		maxHeap = new PriorityQueue<DataRow>(DEFAULT_CAPACITY, new Comparator<DataRow>() {

			@Override
			public int compare(DataRow o1, DataRow o2) {
				return (int) (o2.getTotalCost() - o1.getTotalCost());
			}
		});

		/*
		 * Create the redis-client
		 */
		jedis = new Jedis("localhost", 6379);

		/*
		 * Read set members from redis and add them to the max-heap, ordering
		 * them in decreasing order of total cost
		 */
		populatePriorityQueue();
	}

	private String buildOutputString(DataRow row) {
		StringBuilder sb = new StringBuilder();

		sb.append(row.getCountry());
		sb.append(" ");
		sb.append(row.getTotalCost());
		sb.append(" | ");
		sb.append("(");
		sb.append(row.getVarOverhead() + pricePerTon);
		sb.append(" * ");
		sb.append(tradeVolume);
		sb.append(")");
		sb.append(" + ");
		sb.append(row.getFixedOverhead());

		return sb.toString();
	}

	public void printOutput() {
		while (!maxHeap.isEmpty()) {
			DataRow row = maxHeap.remove();
			System.out.println(buildOutputString(row));
		}
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Syntax: java Fruitpal <fruit> <price per ton> <trade volume>");
			System.exit(1);
		}

		Fruitpal fruitpal = new Fruitpal(args);
		fruitpal.printOutput();
	}
}
