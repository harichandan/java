package hari.fruitpal;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemOutRule;

import redis.clients.jedis.Jedis;

public class FruitpalTest {
	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();

	@Rule
	public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

	@Test(expected = IllegalArgumentException.class)
	public void verifyIllegalArgumentTV() {
		Fruitpal.main(new String[] { "mango", "-2", "405" });
	}

	@Test(expected = IllegalArgumentException.class)
	public void verifyIllegalArgumentPPT() {
		Fruitpal.main(new String[] { "mango", "200", "-1" });
	}

	@Test
	public void verifyInvalidArgumentTV() {
		exit.expectSystemExitWithStatus(2);
		Fruitpal.main(new String[] { "mango", "200", "non-Number" });
	}

	@Test
	public void verifyInvalidArgumentPPT() {
		exit.expectSystemExitWithStatus(2);
		Fruitpal.main(new String[] { "mango", "non-Number", "405" });
	}

	/*
	 *  Verify redis client
	 */
	@Test
	public void verifyRedisKeyValues() {
		Jedis jedis = new Jedis("localhost");
		jedis.sadd("key", "value1");
		jedis.sadd("key", "value2");
		Set<String> set = jedis.smembers("key");
		Assert.assertThat(set, hasItems("value1", "value2"));
		jedis.srem("key", "value1");
		jedis.srem("key", "value2");
		jedis.close();
	}

	/*
	 * Test the example given in the take home document
	 */
	@Test
	public void verifyPrintOutput() {
		Jedis jedis = new Jedis("localhost");
		jedis.sadd("NEWFRUIT", "MX:32:1.24");
		jedis.sadd("NEWFRUIT", "BR:20:1.42");
		Fruitpal fruitpal = new Fruitpal(new String[] { "NEWFRUIT", "405", "53" });
		fruitpal.printOutput();
		assertEquals("BR 22060.1 | (54.42 * 405.0) + 20.0\nMX 21999.2 | (54.24 * 405.0) + 32.0\n",
				systemOutRule.getLog());
		jedis.srem("NEWFRUIT", "MX:32:1.24");
		jedis.srem("NEWFRUIT", "BR:20:1.42");
		jedis.close();

	}
}
