FRUITPAL
--------

There are three components to this submission:
1. FileWatcher.java
2. Fruitpal.java
3. Dockerfile — OPTIONAL

I designed this on a "Write Once Read Many Times" semantic, which makes sense for this application, as we update the data only once a day.

The FileWatcher service uses the WatchService interface to watch the given directory for MODIFY and CREATE events. It looks for files that end with ".txt" in the directory "data/". Whenever a new .txt file or a .txt file with same name but different content is dropped in "data/" directory, the redis database is updated with the new fruit-country data. watchService.take() blocks waiting for file events, so even though the call is inside an infinite loop, it's not a busy wait.

Fruitpal creates a priority queue based on the total cost of the given fruit from countries that trade the fruit. This gives us the ability to extract item with max total cost in O(1) time. Additionally, separating out the ETL from Fruitpal makes it a much better than my previous approach, where I was generating a data frame with all the data for every call to the the Fruitpal script.

I have created a Docker image with redis installed and port 6379 exposed to the host. This is optional, and needs to be used only if you do not already have redis-server installed on your machine.

Usage (Tested on Mac, but should work on Linux)
———————————————————————————————————————————————

1. Start the redis-server: 
	- If redis is installed on the host: redis-server
	- If using the docker image: docker run -p 6379:6379 pharic/state-title

(The docker image is available at: https://hub.docker.com/r/pharic/state-title/)

2. Start the FileWatcher service:
    From the Fruitful directory (project root): 
	$ java -cp ./bin:./* hari.fruitpal.FileWatcher
    To start it as a service:
	$ nohup $ java -cp ./bin:./* hari.fruitpal.FileWatcher &




		
3. Use the Fruitpal class to query. The data/data.txt contains the sample data given in the take home document.
	$ java -cp ./bin:./*.jar hari.fruitpal.Fruitpal	mango 53 405


OUTPUT
——————
BR 22060.1 | (54.42 * 405.0) + 20.0
MX 21998.2 | (54.24 * 405.0) + 31.0

NOTE
————
I have assumed Trade Volume and Price Per Ton to be double, although they seem to be integers in the given document.
