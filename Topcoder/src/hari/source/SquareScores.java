package hari.source;
// SRM 654 - Div2 Easy

// A substring of a string is a contiguous sequence of characters from the string.
// For example, each of the strings "ab", "bcd", and "e" is a substring of "abcde".
// On the other hand, "cba", "ace", and "f" are not substrings of "abcde".
// The score of a string S is the number of ways in which we can select a non-empty
// substring of S such that all characters in the substring are the same. If two
// substrings consist of the same letters but occur at different places in S,
// they are still considered different.
// For example, the score of "aaaba" is 8: there are four occurrences of the
// substring "a", two occurrences of "aa", one occurrence of "aaa", and one of "b".
// On her birthday, Maki got a String s from her friend Niko as a present.
// Calculate and return its score.

// "aaaba" => 8
// "abcdefghijklmnopqrstuvwxyz" => 26
// "zzzxxzz" => 12
// "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" => 5050

public class SquareScores {
	public int findScore(String s) {
		int count = 0;
		int streak = 1;
		for (int i = 0; i < s.length() - 1; i++) {
			if (s.charAt(i) == s.charAt(i + 1)) {
				streak++;
				continue;
			} else {
				count += mult(streak);
				streak = 1;
			}
		}
		count += mult(streak);

		return count;
	}

	public int mult(int n) {
		return (n * (n + 1)) / 2;
	}

	public static void main(String[] args) {
		SquareScores score = new SquareScores();
		System.out.println(score.findScore("aaaba"));
	}

}
