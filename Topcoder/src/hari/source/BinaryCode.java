package hari.source;

/* 
 * SRM Div 2 - 550
 */

public class BinaryCode {
	public String[] decode(String message) {
		boolean flag = false;
		String[] output = new String[2];
		StringBuilder Q = new StringBuilder(message);
		StringBuilder P;
		for (int j = 0; j <= 1; j++) {
			P = new StringBuilder();
			P.append(j);
			for (int i = 0; i < message.length() - 1; i++) {
				int integer;
				if (i >= 1)
					integer = Character.getNumericValue(Q.charAt(i)) - Character.getNumericValue(P.charAt(i))
							- Character.getNumericValue(P.charAt(i - 1));
				else
					integer = Character.getNumericValue(Q.charAt(i)) - Character.getNumericValue(P.charAt(i));

				if (integer < 0) {
					flag = true;
					break;
				}
				P.append(integer);

			}
			if (flag) {
				output[j] = "None";
				flag = false;
			} else {
				output[j] = P.toString();
			}
		}
		return output;
	}

	public static void main(String[] args) {
		BinaryCode b = new BinaryCode();
		String[] out = b.decode("123210122");
		System.out.println(out[0] + "," + out[1]);
	}
}
