package source.hari.spellcheck;

public class WordMEDPair {
	private String word;
	private int minEditDistance;

	WordMEDPair(String word, int med) {
		this.word = word;
		this.minEditDistance = med;
	}
	
	public String getWord() {
		return word;
	}
	
	public int getMED() {
		return minEditDistance;
	}
}