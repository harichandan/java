package source.hari.spellcheck;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

public class SpellChecker {
	Set<String> dictionary;
	PriorityQueue<WordMEDPair> pq;

	SpellChecker() {
		dictionary = new HashSet<>();

		pq = new PriorityQueue<>(5, new Comparator<WordMEDPair>() {
			@Override
			public int compare(WordMEDPair o1, WordMEDPair o2) {
				return Integer.compare(o1.getMED(), o2.getMED());
			}
		});

		File file = new File("data/words.txt");
		try {
			Scanner s = new Scanner(file);
			while (s.hasNextLine())
				dictionary.add(s.nextLine().toLowerCase());

			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private int min(int a, int b, int c) {
		return (a < b) ? ((a < c) ? a : c) : ((b < c) ? b : c);
	}

	private int minimumEditDistance(String s1, String s2) {
		int M = s1.length();
		int N = s2.length();
		int[][] minEditDistance = new int[M + 1][N + 1];

		for (int i = 0; i < M + 1; i++)
			minEditDistance[i][0] = i;

		for (int j = 0; j < N + 1; j++)
			minEditDistance[0][j] = j;

		for (int i = 1; i < M + 1; i++) {
			for (int j = 1; j < N + 1; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1))
					minEditDistance[i][j] = minEditDistance[i - 1][j - 1];
				else
					minEditDistance[i][j] = 1
							+ min(minEditDistance[i][j - 1], minEditDistance[i - 1][j], minEditDistance[i - 1][j]);
			}
		}

		return minEditDistance[M][N];
	}

	public void suggestions(String str) {
		for (String s : dictionary) {
			int distance = minimumEditDistance(str, s);
			if (distance <= 2)
				pq.add(new WordMEDPair(s, distance));
		}

		if (pq.isEmpty()) {
			System.out.println("No suggestions found");
			return;
		}

		int count = 5;

		while (count > 0 && !pq.isEmpty()) {
			count--;
			System.out.println(pq.remove().getWord());
		}
	}

	public static void main(String[] args) {
		SpellChecker spell = new SpellChecker();
		spell.suggestions("hari");
	}
}