package hari.source;

import java.util.Arrays;

public class TripletSum {
  public boolean tripletSum(int x, int[] a) {
    int l = a.length;
    Arrays.sort(a);
    int j,k;
    for (int i=0; i<l; i++) {
      j = i+1;
      k = l-1;
      while (j < k) {
        int sum = a[i] + a[j] + a[k];
        if (sum == x)
          return true;
        else if (sum < x)
          j++;
        else
          k--;
      }
    }
    return false;
  }
  public static void main(String[] args) {
    TripletSum t = new TripletSum();
    System.out.println(t.tripletSum(6, new int[]{1,2,3,4,5}));
    System.out.println(t.tripletSum(13, new int[]{1,2,3,4,5}));
  }

}
