package hari.source;

public class SegregateEvenOdd {
	public void segregate(int[] arr) {
		int left = 0, right = arr.length - 1;
		while (left < right) {
			while (arr[left] % 2 == 0 && left < right)
				left++;
			while (arr[right] % 2 == 1 && left < right)
				right--;
			if (left < right)
				swap(arr, left, right);
		}
	}

	private void swap(int[] arr, int x, int y) {
		int temp = arr[x];
		arr[x] = arr[y];
		arr[y] = temp;
	}

	public static void main(String[] args) {
		int[] arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		new SegregateEvenOdd().segregate(arr);
		for (int x : arr)
			System.out.println(x + " ");
	}
}