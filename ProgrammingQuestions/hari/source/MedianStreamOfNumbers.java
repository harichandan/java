package hari.source;

import java.util.Collections;
import java.util.PriorityQueue;

public class MedianStreamOfNumbers {
	public int median(int[] array) {
		final int DEFAULT_INIT_CAPACITY = 16;
		PriorityQueue<Integer> minHeap = new PriorityQueue<>();
		PriorityQueue<Integer> maxHeap = new PriorityQueue<>(DEFAULT_INIT_CAPACITY, Collections.reverseOrder());

		for (int elem : array) {
			if (minHeap.isEmpty())
				minHeap.add(elem);
			else if (minHeap.peek() <= elem)
				minHeap.add(elem);
			else
				maxHeap.add(elem);

			if (minHeap.size() > maxHeap.size() + 1)
				maxHeap.add(minHeap.remove());
			else if (maxHeap.size() > minHeap.size())
				minHeap.add(maxHeap.remove());
		}

		return (minHeap.size() == maxHeap.size()) ? ((minHeap.peek() + maxHeap.peek()) / 2) : minHeap.peek();
	}

	public static void main(String[] args) {
		MedianStreamOfNumbers med = new MedianStreamOfNumbers();
		System.out.println(med.median(new int[] { 6, 2, 3, 10, 4, 1, 5 })); // Expected
																			// median:
																			// 4
		System.out.println(med.median(new int[] { 3, 1, 2 })); // Expected
																// median: 2

	}
}
