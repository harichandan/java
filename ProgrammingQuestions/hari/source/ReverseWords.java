package hari.source;

public class ReverseWords {
	public static void main(String[] args) {
		String str = "bad is good";
		System.out.println(new ReverseWords().reverseWords(str));
	}

	public String reverseWords(String str) {
		int i, j = 0, l = str.length();
		char[] c = str.toCharArray();
		reverse(c, 0, l);

		for (i = 0; i < l; i++) {
			if (c[i] == ' ') {
				reverse(c, j, i);
				j = i + 1;
			}
		}

		reverse(c, j, l);
		return new String(c);
	}

	private void reverse(char[] c, int start, int end) {
		int i, j;
		for (i = start, j = end - 1; i < (start + end) / 2; i++, j--) {
			char temp = c[i];
			c[i] = c[j];
			c[j] = temp;
		}
	}
}
