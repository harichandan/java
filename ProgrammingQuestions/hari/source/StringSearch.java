package hari.source;

/*
 * Given two strings s1 and s2, return the beginning index
 * of s2 in s1. Return s1 if s2 is not a substring of s1
 */

public class StringSearch {
	public int naiveSearch(String s1, String s2) {
		int i, j;
		int l1 = s1.length();
		int l2 = s2.length();

		if (l2 > l1) {
			System.err.println(s2 + " is not a substring of " + s1);
			return -1;
		}

		for (i = 0; i < l1 - l2 + 1; i++) {
			for (j = 0; j < l2; j++) {
				if (s1.charAt(i + j) != s2.charAt(j))
					break;
			}

			if (j == l2)
				return i;
		}

		return -1;
	}

	public static void main(String[] args) {
		System.out.println(new StringSearch().naiveSearch("harichandan", "aric"));
	}

}
