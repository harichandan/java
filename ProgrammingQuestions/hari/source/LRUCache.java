package hari.source;

import java.util.HashMap;
import java.util.Map;

/* 
 * Create a 3 element cache
 * Eliminate nodes least recently used
 */

public class LRUCache {
	private class Node {
		private int data;
		private Node prev, next;

		Node(int data) {
			this.data = data;
			this.prev = null;
			this.next = null;
		}

		public int getData() {
			return data;
		}

		public Node getPrev() {
			return prev;
		}

		public void setPrev(Node node) {
			prev = node;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node node) {
			next = node;
		}
	}

	private class DLL {
		private Node head;
		private Node tail;
		private int size;

		DLL() {
			head = tail = null;
			size = 0;
		}

		void insert(Node node) {
			if (size == 0) {
				head = node;
				tail = node;
				size++;
				System.out.println("Inserted " + node.getData());
				return;
			}
			tail.setNext(node);
			node.setPrev(tail);
			tail = node;
			System.out.println("Inserted " + node.getData());
			size++;
		}

		@SuppressWarnings("unused")
		void printDLL() {
			Node node = head;
			while (node != null) {
				System.out.println(node.getData());
				node = node.getNext();
			}
		}

		void delete() {
			if (head == null) {
				System.out.println("List is empty");
				return;
			}

			head = head.getNext();
			size--;
		}

		Node getHead() {
			return this.head;
		}

		void setHead(Node node) {
			this.head = node;
		}

		Node getTail() {
			return this.tail;
		}

		void setTail(Node node) {
			this.tail = node;
		}

		int size() {
			return this.size;
		}

	}

	private Map<Integer, Node> map;
	private DLL dll;
	public static final int MAX_CACHE_SIZE = 3;

	LRUCache() {
		map = new HashMap<Integer, Node>();
		dll = new DLL();
	}

	void lookup(Integer data) {
		if (map.containsKey(data)) {
			System.out.println("Cache Hit: " + data);
			Node node = map.get(data);
			/*
			 * Most recently used node is at the tail If the node is not already
			 * the tail, then move it to the tail of the DLL
			 */
			if (node != dll.getTail()) {
				if (node == dll.getHead())
					dll.setHead(node.getNext());

				if (node.getPrev() != null)
					node.getPrev().setNext(node.getNext());
				if (node.getNext() != null)
					node.getNext().setPrev(node.getPrev());
				node.setNext(null);
				node.setPrev(dll.getTail());
				dll.getTail().setNext(node);
				dll.setTail(node);
				return;
			}
		}
		System.out.println("Cache Miss: " + data);
		insert(data);
	}

	void printHashMap() {
		System.out.println(map.values());
	}

	int dllSize() {
		return dll.size();
	}

	void insert(Integer data) {
		if (dll.size() >= MAX_CACHE_SIZE)
			delete();

		Node node = new Node(data);
		dll.insert(node);
		map.put(data, node);
	}

	void delete() {
		/*
		 * Least recently used node is at the head
		 */
		map.remove(dll.getHead().getData());
		dll.delete();
	}

	public static void main(String[] args) {
		LRUCache lru = new LRUCache();
		lru.lookup(3);
		lru.lookup(2);
		lru.lookup(4);
		lru.lookup(3);
		lru.lookup(5);
		lru.lookup(4);
	}
}
