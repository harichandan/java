package hari.source;

public class MinEditDistance {
	/*
	 * Naive Recursive Implementation
	 */
	public int editDistance(String s1, int m, String s2, int n) {
		if (m == 0)
			return n;
		if (n == 0)
			return m;

		if (s1.charAt(m - 1) == s2.charAt(n - 1))
			return editDistance(s1, m - 1, s2, n - 1);

		return 1 + min(editDistance(s1, m, s2, n - 1), // Insertion
				editDistance(s1, m - 1, s2, n), // Deletion
				editDistance(s1, m - 1, s2, n - 1)); // Replace
	}

	/*
	 * Memoized Implementation
	 */
	public int editDistance(String s1, String s2) {
		int M = s1.length(), N = s2.length();
		int d[][] = new int[M + 1][N + 1];
		int i, j;

		for (i = 0; i <= M; i++)
			d[i][0] = i;

		for (j = 0; j <= N; j++)
			d[0][j] = j;

		for (i = 1; i <= M; i++) {
			for (j = 1; j <= N; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1))
					d[i][j] = d[i - 1][j - 1];
				else
					d[i][j] = 1 + min(d[i - 1][j], d[i][j - 1], d[i - 1][j - 1]);
			}
		}

		return d[M][N];
	}

	private int min(int a, int b, int c) {
		return (a < b) ? ((a < c) ? a : c) : ((b < c) ? b : c);
	}

	public static void main(String[] args) {
		MinEditDistance med = new MinEditDistance();
		/*
		 * Calls to Naive Recursive method
		 */
		System.out.println(med.editDistance("sunday", 6, "sundat", 6));
		System.out.println(med.editDistance("", 0, "abc", 3));
		System.out.println(med.editDistance("abc", 3, "", 0));
		System.out.println(med.editDistance("abc", 3, "def", 3));
		/*
		 * Calls to DP method
		 */
		System.out.println(med.editDistance("sunday", "sundat"));
		System.out.println(med.editDistance("", "abc"));
		System.out.println(med.editDistance("abc", ""));
		System.out.println(med.editDistance("abc", "def"));
	}
}
