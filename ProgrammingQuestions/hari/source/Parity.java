package hari.source;

public class Parity {

  static int[] precomputed_parity = new int[256];
   
  public static void main(String[] args) {
    for(int i=0; i<256; i++)
      precomputed_parity[i] = parity(i);
    
    System.out.println(parity(4));
    System.out.println(parity_precomputed(4));
    System.out.println(parity_xor_commuatative(4));
  }

  public static short parity_precomputed(int num) {
    int WORD_SIZE = 8;
    int BIT_MASK = 0xFF;
    return (short) (precomputed_parity[(int) ((num >>> (3*WORD_SIZE)) & BIT_MASK)] 
                  ^ precomputed_parity[(int) ((num >>> (2*WORD_SIZE)) & BIT_MASK)]
                  ^ precomputed_parity[(int) ((num >>> WORD_SIZE) & BIT_MASK)]
                  ^ precomputed_parity[(int) (num & BIT_MASK)]
        );
    
  }

  public static short parity(int num) {
    short par = 0;
    while (num != 0) {
      par ^= 1;
      num &= num-1;
    }
    return par;
  }
  
  public static short parity_xor_commuatative(int num) {
    num ^= num >>> 16;
    num ^= num >>> 8;
    num ^= num >>> 4;
    num ^= num >>> 2;
    num ^= num >>> 1;
    return (short) (num & 0x1);
  }
}
