package hari.source;

public class ReverseVowelsInString {
	private boolean isVowel(char a) {
		return (a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u' || a == 'A' || a == 'E' || a == 'I' || a == 'O'
				|| a == 'U');
	}

	private void swap(char[] arr, int i, int j) {
		char temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	public String reverseVowels(String s) {
		char[] charArray = s.toCharArray();
		int i = 0, j = s.length() - 1;

		while (i < j) {
			while (!isVowel(charArray[i]))
				i++;

			while (!isVowel(charArray[j]))
				j--;

			if (i < j) {
				swap(charArray, i, j);
				i++;
				j--;
			}
		}

		return new String(charArray);
	}

	public static void main(String[] args) {
		String s = "harecihoru";
		System.out.println(s + " : " + new ReverseVowelsInString().reverseVowels(s));
	}

}
