package hari.source;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ArrayPairSum {
	public static void main(String[] args) {
		int[] A = new int[] { 1, 2, 3, 4, 5 };

		List<List<Integer>> l = arrayPairSum(A, 6);
		Iterator<List<Integer>> i = l.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}

	}

	public static List<List<Integer>> arrayPairSum(int[] A, int sum) {
		List<List<Integer>> list = new ArrayList<>();
		int low = 0, high = A.length - 1;
		while (low < high) {
			int s = A[low] + A[high];
			if (s == sum) {
				List<Integer> l = new ArrayList<>();
				l.add(A[low++]);
				l.add(A[high--]);
				list.add(l);
			} else if (s < sum)
				low++;
			else
				high--;
		}
		return list;
	}

	// O(n): Works only for sorted arrays
	public static void printArrayPairSum(int[] A, int sum) {
		int count = 0;
		for (int i = 0, j = A.length - 1; i < j;) {
			if (A[i] + A[j] == sum) {
				System.out.println(A[i++] + " " + A[j--]);
				count++;
			} else if (A[i] + A[j] < sum)
				i++;
			else
				j--;
		}
		if (count == 0)
			System.out.println("No pairs found which add up to " + sum);
	}

	// O(n): Works for both sorted and unsorted arrays
	public static void arraySumSets(int[] A, int sum) {
		int count = 0;

		Set<Object> s = new HashSet<Object>();

		for (int item : A) {
			if (s.contains(sum - item)) {
				System.out.println(item + " " + (sum - item));
				count++;
			} else
				s.add(item);
		}

		if (count == 0)
			System.out.println("No pairs found which add up to " + sum);
	}
}
