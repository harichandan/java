package hari.source;

import java.util.HashSet;
import java.util.Set;

public class TwoSum {
  public void twoSumSorted(Integer[] arr, int target) {
    for (int i = 0, j = arr.length - 1; i < j;) {
      int sum = arr[i] + arr[j];
      if (sum == target) {
        System.out.println(arr[i] + " " + arr[j]);
        return;
      }
      else if (sum < target)
        i++;
      else 
        j--;
    }
    System.out.println("No two numbers in the given array add up to " + target);
  }
  
  public void twoSumUnsorted(Integer[] array, int target) {
    Set<Integer> set = new HashSet<Integer>();
    for (int i=0; i< array.length; i++) {
      if (set.contains(target - array[i])) {
        System.out.println(target - array[i] + " " + array[i]);
        return;
      }
      set.add(array[i]);
    }
    System.out.println("No two numbers in the given array add up to " + target);
  }

  public static void main(String[] args) {
    Integer[] array = new Integer[]{1,2,3,4,5,6,8,13,17,19};
    new TwoSum().twoSumSorted(array, 15);
    Integer[] anotherArray = new Integer[]{0,8,3,5,2,7,1};
    new TwoSum().twoSumUnsorted(anotherArray, 10);
  }

}
