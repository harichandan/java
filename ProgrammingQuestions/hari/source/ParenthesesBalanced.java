package hari.source;


public class ParenthesesBalanced {
  public boolean parenBalanced(String str) {
    int count = 0;
    for (int i=0; i<str.length(); i++) {
      if (str.charAt(i) == '(')
        count++;
      
      else if (str.charAt(i) == ')') {
        if (count == 0)
          return false;
        count--;
      }
    }
    
    return count == 0;
  }
  
  public static void main(String[] args) {
    ParenthesesBalanced p = new ParenthesesBalanced();
    System.out.println("(()) : " + p.parenBalanced("(())"));
    System.out.println("()() : " + p.parenBalanced("()()"));
    System.out.println("(())) : " + p.parenBalanced("(()))"));
    System.out.println("()()( : " + p.parenBalanced("()()("));
  }

}
