package hari.source;

import java.util.ArrayList;
import java.util.List;

public class Bigrams {
	public List<String> bigrams(String str) {
		String[] splitArray = str.split(" ");
		List<String> list = new ArrayList<>();

		for (int i = 0; i < splitArray.length - 1; i++)
			list.add(splitArray[i] + " " + splitArray[i + 1]);

		return list;
	}

	public static void main(String[] args) {
		System.out.println(new Bigrams().bigrams("There is a red table under a red book"));
	}
}
