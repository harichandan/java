package hari.source;

public class SearchColRowSortedMatrix {
  public int[] search(int[][] matrix, int key) {
    int M = matrix.length; // No. of rows
    int N = matrix[0].length; // No. of cols

    int i = 0, j = N - 1;
    while (i < M && j >= 0) {
      if (matrix[i][j] == key)
        return (new int[] { i, j });
      else if (matrix[i][j] < key)
        i++;
      else
        j--;
    }
    return new int[] { -1, -1 };
  }

  public static void main(String[] args) {
    int mat[][] = new int[][] { { 10, 20, 30, 40 }, { 15, 25, 35, 45 },
        { 27, 29, 37, 48 }, { 32, 33, 39, 50 }, };

    int[] indices = new SearchColRowSortedMatrix().search(mat, 37);
    System.out.println(indices[0] + "," + indices[1]);
  }
}