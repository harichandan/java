package hari.source;

public class RunLengthEncoding {

  public static void main(String[] args) {
    RunLengthEncoding r = new RunLengthEncoding();
    System.out.println(r.RLE("aaaabbbccccccd"));
    System.out.println(r.RLD("a10b2c4"));
  }

  public String RLE(String s) {
    int i, count = 1;
    StringBuilder str = new StringBuilder();
    for (i = 1; i <= s.length(); i++) {
      if (i == s.length() || s.charAt(i) != s.charAt(i - 1)) {
        str.append(s.charAt(i - 1));
        str.append(count);
        count = 1;
      } else
        count++;
    }
    return str.toString();
  }

  public String RLD(String s) {
    int count = 0;
    char c = s.charAt(0);
    StringBuilder str = new StringBuilder();
    for (int i = 1; i <= s.length(); i++) {

      if (i != s.length() && Character.isDigit(s.charAt(i))) {
        count = count * 10 + s.charAt(i) - '0';
      }

      else if (i == s.length() || Character.isAlphabetic(s.charAt(i))) {
        while (count > 0) {
          str.append(c);
          count--;
        }
        if (i < s.length())
          c = s.charAt(i);
      }
    }
    return str.toString();
  }

}
