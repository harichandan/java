package hari.source;

import java.util.LinkedList;
import java.util.Queue;

class Node {
	int data;
	Node left, right;

	Node(int data) {
		this.data = data;
		left = null;
		right = null;
	}
}

public class BST {
	private Node root;

	BST() {
		root = null;
	}

	private Node insert(Node node, int data) {
		if (node == null) {
			node = new Node(data);
			return node;
		}

		if (data <= node.data)
			node.left = insert(node.left, data);
		else
			node.right = insert(node.right, data);

		return node;
	}

	public void insert(int data) {
		root = insert(root, data);
	}

	public Node search(Node root, int data) {
		if (root == null || root.data == data)
			return root;

		if (data < root.data)
			return search(root.left, data);
		else
			return search(root.right, data);
	}

	public Node getRoot() {
		return root;
	}

	public void inorderTraversal(Node node) {
		if (node == null)
			return;
		inorderTraversal(node.left);
		System.out.print(node.data + " ");
		inorderTraversal(node.right);
	}

	public boolean validateBST(Node root) {
		return validateBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	private boolean validateBST(Node root, int min, int max) {
		if (root == null)
			return true;

		if (root.left != null && root.data < root.left.data)
			return false;

		if (root.right != null && root.data >= root.right.data)
			return false;

		return validateBST(root.left, min, root.data) && validateBST(root.right, root.data, max);
	}

	public void levelOrderTraversal() {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			Node n = queue.remove();
			System.out.print(n.data + " ");
			if (n.left != null)
				queue.add(n.left);
			if (n.right != null)
				queue.add(n.right);
		}
		System.out.println();
	}

	public int getCountInRange(Node root, int low, int high) {
		if (root == null)
			return 0;

		if (root.data >= low && root.data <= high)
			return 1 + getCountInRange(root.left, low, high) + getCountInRange(root.right, low, high);

		else if (root.data < low)
			return getCountInRange(root.right, low, high);
		else
			return getCountInRange(root.left, low, high);
	}

	public int levelDiffEvenOdd(Node root) {
		if (root == null)
			return 0;
		return root.data - levelDiffEvenOdd(root.left) - levelDiffEvenOdd(root.right);
	}

	public int getHeight(Node node) {
		if (node == null)
			return 0;

		int left = getHeight(node.left);
		int right = getHeight(node.right);

		return Math.max(left, right) + 1;
	}

	public static void main(String[] args) {
		BST bst = new BST();
		bst.insert(4);
		bst.insert(2);
		bst.insert(1);
		bst.insert(3);
		bst.insert(6);
		bst.insert(5);
		bst.insert(7);

		/*
		 * Inorder Traversal
		 */
		bst.inorderTraversal(bst.getRoot());
		System.out.println();

		/*
		 * Validate BST
		 */
		System.out.println("Tree Valid? : " + bst.validateBST(bst.getRoot()));

		/*
		 * Search
		 */
		Node searchNode = bst.search(bst.getRoot(), 4);
		if (searchNode != null)
			System.out.println("Found node with data = 4");

		/*
		 * Level Order Traversal
		 */
		bst.levelOrderTraversal();
		System.out.println("No. of keys in range 2-5 (inclusive): " + bst.getCountInRange(bst.getRoot(), 2, 5));

		System.out.println(bst.levelDiffEvenOdd(bst.getRoot()));
		
		/*
		 * Height of BST
		 */
		System.out.println("Height of tree: " + bst.getHeight(bst.getRoot()));
		
	}
}
