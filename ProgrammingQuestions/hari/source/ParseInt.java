package hari.source;

public class ParseInt {
	public static void main(String[] args) {
		String num = "-000123";
		System.out.println(new ParseInt().parseInt(num));
	}

	public int parseInt(String s) {
		int num = 0;
		int sign = (s.charAt(0) == '-' ? 1 : 0);

		for (int i = sign; i < s.length(); i++) {
			if (s.charAt(i) < '0' || s.charAt(i) > '9') {
				System.out.println("Invalid Character found!");
				System.exit(-1);
			}
			num = num * 10 + (s.charAt(i) - '0');
		}

		return (sign == 0) ? num : -num;
	}

}
