package hari.source;

public class MaxHeap {
	public static void main(String[] args) {
		int max_heap[] = { 2, 1, 7, 6, 5, 4, 3, 9, 8, 13, 12, 15, 16, 14, 17, 11 };

		System.out.println("Max Heap Size: " + max_heap.length);
		for (int i = (max_heap.length / 2) - 1; i >= 0; i--) {
			max_heap = max_heapify(max_heap, i);
		}

		for (int i = 0; i < max_heap.length; i++)
			System.out.print(max_heap[i] + " ");
	}

	@SuppressWarnings("unused")
	private static int parent(int index) {
		return (int) (Math.ceil(index / 2) - 1);
	}

	private static int left(int index) {
		return 2 * index + 1;
	}

	private static int right(int index) {
		return 2 * index + 2;
	}

	public static int[] max_heapify(int A[], int root_index) {
		int l = left(root_index);
		int r = right(root_index);

		int largest = root_index;
		if (l < A.length && A[l] > A[root_index])
			largest = l;
		else
			largest = root_index;
		if (r < A.length && A[r] > A[largest])
			largest = r;
		if (largest != root_index) {
			int temp = A[largest];
			A[largest] = A[root_index];
			A[root_index] = temp;
			max_heapify(A, largest);
		}
		return A;
	}
}
