package hari.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/* 
 * Integer -> String KV Store
 * --------------------------
 * Design a key-value store to support methods like get, put and getRandom.
 * getRandom -> Should generate a random number and return the key-value 
 * stored at this random number
 */

public class KVStore {
	private Map<Integer, String> kvstore;
	private List<Integer> keys;

	public KVStore() {
		kvstore = new HashMap<Integer, String>();
		keys = new ArrayList<Integer>();
	}

	public String get(int key) {
		return kvstore.get(key);
	}

	public void set(int key, String value) {
		kvstore.put(key, value);
		keys.add(key);
	}

	public String getRandom() {
		if (keys.size() == 0) {
			System.err.println("No elements in KV store");
			System.exit(-1);
		}
		Random r = new Random();
		return kvstore.get(keys.get(r.nextInt(keys.size())));
	}

	public static void main(String[] args) {
		KVStore kv = new KVStore();
		kv.set(1, "one");
		kv.set(3, "three");
		kv.set(5, "five");
		kv.set(7, "seven");
		kv.set(9, "nine");
		System.out.println(kv.get(9));
		System.out.println(kv.get(4));
		System.out.println(kv.getRandom());
	}
}