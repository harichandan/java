package hari.source;

import java.util.HashMap;
import java.util.Map;

public class RomanToInt {
  @SuppressWarnings("serial")
  public int roman_to_int(String s) {

    Map<Character, Integer> m = new HashMap<Character, Integer>() {
      {
        put('I', 1);
        put('V', 5);
        put('X', 10);
        put('L', 50);
        put('C', 100);
        put('D', 500);
        put('M', 1000);
      }
    };
    
    int num = m.get(s.charAt(s.length() - 1));

    for (int i = s.length() - 2; i >= 0; i--) {
      if (s.charAt(i) < s.charAt(i + 1)) {
        num -= m.get(s.charAt(i));
      } else
        num += m.get(s.charAt(i));
    }
    return num;
  }

}
