package hari.source;

public class NQueens {
	public void printBoard(int[][] board) {
		int numRows = board.length;
		int numCols = numRows; // Square board!

		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++)
				System.out.print(" " + board[i][j] + " ");
			System.out.println();
		}

	}

	private boolean isSafe(int[][] board, int row, int col) {
		/*
		 * Check if a queen already exists in any of the previous columns in the
		 * same row
		 */
		for (int i = 0; i < col; i++)
			if (board[row][i] == 1)
				return false;

		/*
		 * Check if a queen already exists in the upper left diagonal
		 */
		for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--)
			if (board[i][j] == 1)
				return false;

		/*
		 * Check if a queen already exists in the lower left diagonal
		 */
		for (int i = row + 1, j = col - 1; i < board.length && j >= 0; i++, j--)
			if (board[i][j] == 1)
				return false;

		return true;
	}

	public boolean solveNQueen(int[][] board, int col) {
		if (col >= board.length)
			return true;

		for (int i = 0; i < board.length; i++) {
			if (isSafe(board, i, col)) {
				board[i][col] = 1;

				if (solveNQueen(board, col + 1))
					return true;

				board[i][col] = 0;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		NQueens nq = new NQueens();
		int[][] board = new int[8][8];
		if (!nq.solveNQueen(board, 0)) {
			System.err.println("Solution doesn't exist");
			System.exit(1);
		}
		nq.printBoard(board);
	}
}