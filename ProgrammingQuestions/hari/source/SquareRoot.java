package hari.source;

public class SquareRoot {

  public static void main(String[] args) {
    System.out.println("" + sqrt(3));
  }

  public static double sqrt(double n) {
    if (n < 0) {
      System.out.println("Invalid input. n should be non-negative");
    }
    double mid, low = 0, high = n;
    while (low <= high) {
      mid = (low + high) / 2;
      if (accurate(mid, n))
        return mid;
      else if (mid * mid < n)
        low = mid;
      else
        high = mid;
    }
    return -1;
  }

  public static boolean accurate(double estimate, double n) {
    return (abs(estimate * estimate - n) / n <= 0.000001);
  }

  public static double abs(double n) {
    return ((n > 0) ? n : -n);
  }

}
