package hari.source;

public class OrderStatistics {
	public int kthLargestElement(int[] array, int k) {
		return kthLargestElementHelper(array, 0, array.length - 1, k);
	}

	private int kthLargestElementHelper(int[] array, int low, int high, int k) {
		if (k > 0 && k <= high - low + 1) {
			int pos = partition(array, low, high);
			if (pos - low == k - 1)
				return array[pos];
			else if (pos - low < k - 1)
				return kthLargestElementHelper(array, pos + 1, high, k - pos + low);
			else
				return kthLargestElementHelper(array, low, pos - 1, k);
		}
		return Integer.MIN_VALUE;
	}

	private int partition(int[] array, int low, int high) {
		int q = array[high];
		int i = low;

		for (int j = low; j < high; j++) {
			if (array[j] < q) {
				swap(array, i, j);
				i++;
			}
		}
		swap(array, i, high);

		return i;
	}

	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	public static void main(String[] args) {
		OrderStatistics ord = new OrderStatistics();
		int[] array = new int[] { 7, 8, 4, 1, 5, 2, 3, 6 };
		System.out.println(ord.kthLargestElement(array, 2));
	}
}
