package hari.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveOdd {
	public List<Integer> removeOdd(ArrayList<Integer> arr) {
		int evenIndex = 0;
		int listLength = arr.size();
		for (int i = 0; i < listLength; i++) {
			if (arr.get(i) % 2 == 0) {
				arr.set(evenIndex++, arr.get(i));
			} else
				continue;
		}

		return arr.subList(0, evenIndex);
	}

	public int[] removeOdd(int[] array) {
		int count = 0;

		for (int i = 0; i < array.length; i++) {
			if (!isOdd(array[i]))
				array[count++] = array[i];
		}

		return Arrays.copyOfRange(array, 0, count);
	}

	private boolean isOdd(int i) {
		return i % 2 == 1;
	}

	public static void main(String[] args) {
		ArrayList<Integer> l = new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
		System.out.println(new RemoveOdd().removeOdd(l));

		int[] array = new int[] { 3, 2, 4, 1, 5, 7, 6 };
		int[] noOddArray = new RemoveOdd().removeOdd(array);
		for (int i = 0; i < noOddArray.length; i++)
			System.out.print(noOddArray[i] + " ");

	}

}
