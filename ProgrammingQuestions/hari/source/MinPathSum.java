package hari.source;

public class MinPathSum {
	public int minPathSum(int i, int j, int[][] grid, int m, int n) {
		if (i == m - 1 && j == n - 1)
			return grid[i][j];

		if (i < m - 1 && j < n - 1) {
			int sum1 = grid[i][j] + minPathSum(i + 1, j, grid, m, n);
			int sum2 = grid[i][j] + minPathSum(i, j + 1, grid, m, n);
			return Math.min(sum1, sum2);
		}

		else if (i < m - 1)
			return grid[i][j] + minPathSum(i + 1, j, grid, m, n);
		else
			return grid[i][j] + minPathSum(i, j + 1, grid, m, n);

	}

	public int minPathSum(int[][] grid) {
		int m = grid.length;
		int n = grid[0].length;

		int[][] dp = new int[m][n];
		dp[0][0] = grid[0][0];

		for (int i = 1; i < n; i++)
			dp[0][i] = dp[0][i - 1] + grid[0][i];
		for (int j = 1; j < m; j++)
			dp[j][0] = dp[j - 1][0] + grid[j][0];

		for (int i = 1; i < m; i++)
			for (int j = 1; j < n; j++)
				dp[i][j] = grid[i][j] + Math.min(dp[i - 1][j], dp[i][j - 1]);

		return dp[m - 1][n - 1];
	}

	public static void main(String[] args) {
		MinPathSum pathSum = new MinPathSum();
		int[][] grid = new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		System.out.println("Recursive Solution: " + pathSum.minPathSum(0, 0, grid, grid.length, grid[0].length));
		System.out.println("DP Solution: " + pathSum.minPathSum(grid));
	}

}
