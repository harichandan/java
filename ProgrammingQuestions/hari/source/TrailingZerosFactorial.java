package hari.source;

public class TrailingZerosFactorial {
	public int numZerosTrail(int n) {
		int count = 0;

		for (int i = 5; n / i >= 1; i *= 5)
			count += n / i;

		return count;
	}

	public static void main(String[] args) {
		TrailingZerosFactorial t = new TrailingZerosFactorial();
		System.out.println(t.numZerosTrail(3)); // 6 => 0
		System.out.println(t.numZerosTrail(5)); // 120 => 1
		System.out.println(t.numZerosTrail(10)); // 3628800 => 2
		System.out.println(t.numZerosTrail(25));
	}

}