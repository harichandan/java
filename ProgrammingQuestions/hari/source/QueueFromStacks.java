package hari.source;

import java.util.LinkedList;
import java.util.Deque;

public class QueueFromStacks {
	Deque<Integer> s1, s2;
	
	void add(Integer elem) {
		shiftStacks(true);
		s2.push(elem);
	}
	
	Integer remove() {
		shiftStacks(false);
		return s1.isEmpty() ? null : s1.pop();
	}

	// When flag = true, shift q1 to q2
	// When flag = false, shift q2 to q1
	void shiftStacks(boolean flag) {
		Deque<Integer> from, to;
		if (flag) {
			from = s1;
			to = s2;
		} else {
			from = s2;
			to = s1;
		}
		
		while (!from.isEmpty()) {
			to.push(from.pop());
		}
	}
	
	QueueFromStacks() {
		s1 = new LinkedList<>();
		s2 = new LinkedList<>();
	}

	public static void main(String[] args) {
		QueueFromStacks q = new QueueFromStacks();
		q.add(1);
		q.add(2);
		q.add(3);
		System.out.println(q.remove());
		System.out.println(q.remove());
		System.out.println(q.remove());
		System.out.println(q.remove());
	}

}
