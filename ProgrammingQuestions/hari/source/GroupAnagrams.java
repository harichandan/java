package hari.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupAnagrams {
	private void generateMapOfAnagrams(Map<String, List<String>> map, String s) {
		char[] array = s.toCharArray();
		Arrays.sort(array);
		String sortedString = new String(array);
		if (map.containsKey(sortedString))
			map.get(sortedString).add(s);
		else
			map.put(sortedString, new ArrayList<>(Arrays.asList(s)));
	}

	public List<List<String>> groupAnagrams(String[] strs) {
		List<List<String>> anagramList = new ArrayList<>();
		Map<String, List<String>> map = new HashMap<>();

		for (String str : strs)
			generateMapOfAnagrams(map, str);

		anagramList.addAll(map.values());
		return anagramList;
	}

	public static void main(String[] args) {
		String[] strs = { "eat", "tea", "tan", "ate", "nat", "bat" };
		System.out.println(new GroupAnagrams().groupAnagrams(strs));
	}

}
