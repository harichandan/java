package hari.source;

import java.util.Hashtable;

public class FibonacciMemoization {
  public static Hashtable<Integer, Integer> h = new Hashtable<Integer, Integer>();

  public static void main(String[] args) {
    FibonacciMemoization f = new FibonacciMemoization();
    h.put(0, 0);
    h.put(1, 1);
    int x = f.fibonacci(1000);
    System.out.println("" + x);

  }

  int fibonacci(int n) {
    if (h.get(n) != null)
      return h.get(n);
    int x = fibonacci(n - 1) + fibonacci(n - 2);
    h.put(n, x);
    return x;

  }
}
