package hari.source;

public class BinarySearch {
  public int binSearch(int[] A, int low, int high, int key) {
    while (low <= high) {
      int mid = (low + high) / 2;

      if (A[mid] == key)
        return mid;
      else if (A[mid] < key)
        low = mid + 1;
      else
        high = mid - 1;
    }
    return -1;
  }
}

