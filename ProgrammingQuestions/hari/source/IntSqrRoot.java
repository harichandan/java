package hari.source;

public class IntSqrRoot {

	public static void main(String[] args) {
		IntSqrRoot i = new IntSqrRoot();
		System.out.println(i.sqrRoot(8));
	}

	public int sqrRoot(int n) {
		int low = 0, high = n, mid;
		while (low <= high) {
			mid = (low + high) / 2;
			if (mid * mid <= n)
				low = mid + 1;
			else
				high = mid - 1;
		}
		return low - 1;
	}
}
