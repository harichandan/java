package hari.source;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SieveOfEratosthenes {
	public static void main(String[] args) {
		int n = 20;
		primes(n);
		List<Integer> prime_numbers = primes_list(n);
		System.out.println("\nThe list of prime numbers upto " + n + " is: ");
		for (int i = 0; i < prime_numbers.size(); i++)
			System.out.print(prime_numbers.get(i) + " ");

	}

	public static void primes(int n) {
		boolean[] prime = new boolean[n + 1];
		int i, j;
		for (i = 0; i <= n; i++)
			prime[i] = true;

		prime[0] = false;
		prime[1] = false;

		for (i = 2; i <= n; i++) {
			if (prime[i]) {
				for (j = 2 * i; j <= n; j += i)
					prime[j] = false;
			}
		}
		System.out.println("The primes upto " + n + " are: ");
		for (i = 0; i <= n; i++) {
			if (prime[i])
				System.out.print(i + " ");
		}
	}

	public static List<Integer> primes_list(int n) {
		List<Integer> primes = new ArrayList<>();
		List<Boolean> numbers = new ArrayList<>(Collections.nCopies(n + 1, true));
		int i, j;
		numbers.set(0, false);
		numbers.set(1, false);
		for (i = 2; i <= n; i++) {
			if (numbers.get(i)) {
				primes.add(i);
				for (j = i; j <= n; j += i)
					numbers.set(j, false);
			}
		}
		return primes;
	}
}
