package hari.source;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class OrderByFrequency {
	public String orderByFrequency(String str) {
		StringBuilder s = new StringBuilder();
		SortedMap<Character, Integer> map = new TreeMap<Character, Integer>();
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			Integer val = map.get(ch);
			map.put(ch, 1 + ((val == null) ? 0 : val));
		}
		System.out.println(map);
		for (Map.Entry<Character, Integer> entry : map.entrySet()) {
			Character c = entry.getKey();
			Integer count = entry.getValue();
			while (count > 0) {
				s.append(c);
				count--;
			}
		}

		return s.toString();
	}

	public static void main(String[] args) {
		OrderByFrequency order = new OrderByFrequency();
		System.out.println(order.orderByFrequency("BLOOMBERG"));
	}
}
