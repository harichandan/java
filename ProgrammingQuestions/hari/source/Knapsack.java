package hari.source;

public class Knapsack {
	private int max(int a, int b) {
		return (a > b) ? a : b;
	}

	public int knapsack(int W, int[] weights, int[] values, int n) {
		if (n == 0 || W == 0)
			return 0;

		/*
		 * If weight of nth object is greater than the capacity, do not include
		 * the object in the knapsack
		 */
		if (weights[n - 1] > W)
			return knapsack(W, weights, values, n - 1);

		/*
		 * Return the max of values with nth object included and excluded
		 */
		return max(values[n - 1] + knapsack(W - weights[n - 1], weights, values, n - 1),
				knapsack(W, weights, values, n - 1));

	}

	public int knapsack(int W, int[] weights, int[] values) {
		int M = values.length;

		int[][] dp = new int[M + 1][W + 1];

		for (int i = 0; i <= M; i++)
			dp[i][0] = 0;

		for (int w = 0; w <= W; w++)
			dp[0][w] = 0;

		for (int i = 1; i <= M; i++) {
			for (int w = 1; w <= W; w++) {
				if (weights[i - 1] > w)
					dp[i][w] = dp[i - 1][w];
				else
					dp[i][w] = max(values[i - 1] + dp[i - 1][w - weights[i - 1]], dp[i - 1][w]);
			}
		}

		return dp[M][W];
	}

	public static void main(String[] args) {
		Knapsack k = new Knapsack();
		int W = 50;
		int[] values = new int[] { 60, 100, 120 };
		int[] weights = new int[] { 10, 20, 30 };
		System.out.println(k.knapsack(W, weights, values, values.length));
		System.out.println(k.knapsack(W, weights, values));
	}
}
