package hari.source;

// Print the sorted order given a level order traversal
// of a BST in an array

public class PrintSortedArrayTree {
  public void printSorted(int[] arr, int start, int end) {
    if (start > end)
      return;

    printSorted(arr, 2 * start + 1, end);
    System.out.println(arr[start]);
    printSorted(arr, 2 * start + 2, end);
  }

  public void printSorted(int[] arr) {
    printSorted(arr, 0, arr.length - 1);
  }

  public static void main(String[] args) {
    int[] arr = new int[] { 4, 2, 5, 1, 3 };
    new PrintSortedArrayTree().printSorted(arr);
  }
}