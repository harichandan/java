package hari.source;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

public class ClientServerCalculator extends Thread {

  public void server() throws IOException {
    ServerSocket server = new ServerSocket(4444);
    Socket serverSocket = server.accept();
    System.out.println("Client connected");
    DataInputStream in = new DataInputStream(serverSocket.getInputStream());
    StringTokenizer s = new StringTokenizer(in.readUTF());
    int firstNumber = Integer.parseInt(s.nextToken());
    String operation = s.nextToken();
    int secondNumber = Integer.parseInt(s.nextToken());
    int result = Integer.MIN_VALUE;
    switch (operation) {
    case "+":
      result = firstNumber + secondNumber;
      break;
    case "-":
      result = firstNumber - secondNumber;
      break;
    case "*":
      result = firstNumber * secondNumber;
      break;
    case "/":
      result = firstNumber / secondNumber;
      break;
    default:
      System.out.println("Invalid operation");
    }
    DataOutputStream out = new DataOutputStream(serverSocket.getOutputStream());
    out.writeInt(result);
    server.close();
  }

  public void run() {
    try {
      System.out.println("Starting Server");
      server();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public void client(String str) throws IOException {
    Socket client = new Socket("localhost", 4444);
    DataOutputStream out = new DataOutputStream(client.getOutputStream());
    out.writeUTF(str);
    DataInputStream in = new DataInputStream(client.getInputStream());
    System.out.println(str + " = " + in.readInt());
    client.close();
  }

  public static void main(String[] args) throws IOException {
    Thread t = new ClientServerCalculator();
    t.start();
    ClientServerCalculator client = new ClientServerCalculator();
    client.client("3 / 2");
  }

}
