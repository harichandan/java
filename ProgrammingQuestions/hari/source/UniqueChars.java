package hari.source;

public class UniqueChars {

  public int checkUnique(String str) {
    int[] uniqueChar = new int[26];

    for (int i = 0; i < str.length(); i++) {
      if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z')
        uniqueChar[str.charAt(i) - 'a']++;
      else {
        System.out
            .println("Invalid output detected. String should contain characters 'a'to'z'");
        return -2;
      }

    }

    for (int count:uniqueChar)
      if (count > 1)
        return -1;
    return 0;
  }

}
