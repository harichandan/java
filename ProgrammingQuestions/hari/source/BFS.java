package hari.source;

import java.util.LinkedList;
import java.util.List;

public class BFS {
	public void breadthFirstSearch(Graph graph, int source) {
		LinkedList<Integer> queue = new LinkedList<>();
		boolean[] visited = new boolean[graph.totalVertices()];
		queue.add(source);
		visited[source] = true;

		while (!queue.isEmpty()) {
			int node = queue.remove();
			System.out.println(node);
			List<Integer> adjList = graph.getNeighbors(node);

			for (int neighbor : adjList) {
				if (!visited[neighbor]) {
					visited[neighbor] = true;
					queue.add(neighbor);
				}
			}
		}
	}

	public static void main(String[] args) {
		Graph graph = new Graph(6);
		graph.addEdge(0, 1);
		graph.addEdge(0, 5);
		graph.addEdge(1, 2);
		graph.addEdge(1, 4);
		graph.addEdge(2, 3);
		graph.addEdge(2, 5);
		new BFS().breadthFirstSearch(graph, 0);
	}

}
