package hari.source;

import java.util.HashMap;
import java.util.Map;

public class IsomorphicStrings {
	/*
	 * Check if given two strings are isomorphic abc -> xyz => True abb -> two
	 * => False
	 */

	public boolean areIsomorphic(String s1, String s2) {
		int l1 = s1.length();
		int l2 = s2.length();

		if (l1 != l2)
			return false;
		Map<Character, Character> map = new HashMap<>();
		for (int i = 0; i < l1; i++) {
			char c1 = s1.charAt(i);
			char c2 = s2.charAt(i);
			if (map.containsKey(c1)) {
				if (map.get(c1) != c2)
					return false;
			} else if (map.containsValue(c2)) {
				return false;
			} else {
				map.put(c1, c2);
			}

		}

		return true;
	}

	public static void main(String[] args) {
		IsomorphicStrings iso = new IsomorphicStrings();
		System.out.println("abc -> xyz: " + iso.areIsomorphic("abc", "xyz"));
		System.out.println("abc -> xyz: " + iso.areIsomorphic("abb", "two"));
	}

}
