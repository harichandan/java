package hari.source;

public class CountCharOccurrences {
  public static void main(String args[]) {
    String s = "harichandan";
    countCharOcc(s);
  }

  public static void countCharOcc(String s) {
    int[] arr = new int[26];
    for (int i = 0; i < s.length(); i++) {
      arr[s.charAt(i) - 'a']++;
    }

    System.out.println("The character occurrences are as follows: ");
    for (int i = 0; i < 26; i++) {
      if(arr[i] > 0)
        System.out.format("<%c,%d>\n", 'a'+ i, arr[i]);
    }
  }
}