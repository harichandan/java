package hari.source;

public class LongestCommonSubsequence {
	// Naive Recursive Implementation
	int lcs(String s1, String s2, int m, int n) {
		if (m == 0 || n == 0)
			return 0;

		if (s1.charAt(m - 1) == s2.charAt(n - 1))
			return 1 + lcs(s1, s2, m - 1, n - 1);
		else
			return Math.max(lcs(s1, s2, m, n - 1), lcs(s1, s2, m - 1, n));
	}

	// Memoized Implementation
	int lcs(String s1, String s2) {
		int m = s1.length(), n = s2.length();
		int[][] lcs = new int[m + 1][n + 1];

		for (int i = 0; i <= m; i++)
			lcs[i][0] = 0;
		for (int i = 0; i <= n; i++)
			lcs[0][i] = 0;

		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1))
					lcs[i][j] = 1 + lcs[i - 1][j - 1];
				else
					lcs[i][j] = Math.max(lcs[i - 1][j], lcs[i][j - 1]);
			}
		}

		return lcs[m][n];
	}

	public static void main(String[] args) {
		LongestCommonSubsequence lcs = new LongestCommonSubsequence();
		String s1 = "ABCDGH";
		String s2 = "AEDFHR";
		String s3 = "AGGTAB";
		String s4 = "GXTXAYB";
		// 3 => ADH
		System.out.println(lcs.lcs(s1, s2));
		System.out.println(lcs.lcs(s1, s2, s1.length(), s2.length()));
		// 4 => GTAB
		System.out.println(lcs.lcs(s3, s4));
		System.out.println(lcs.lcs(s3, s4, s3.length(), s4.length()));
	}
}