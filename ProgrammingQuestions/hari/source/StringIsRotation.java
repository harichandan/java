package hari.source;

// Check if one string is a rotation of another

public class StringIsRotation {

  public static void main(String[] args) {
    StringIsRotation s = new StringIsRotation();
    String s1 = "hari";
    String s2 = "hari";
    boolean x = s.isPermutation(s1, s2);
    System.out.println("" + x);
  }

  private boolean isPermutation(String s1, String s2) {
    if (s1.length() != s2.length())
      return false;
    s1 = s1 + s1;
    return isSubstring(s1, s2);
  }

  private boolean isSubstring(String s1, String s2) {
    for (int i = 0; i < s2.length(); i++) {
      if (s1.substring(i, i + s2.length()).equals(s2)) {
        return true;
      }
    }
    return false;
  }
}
