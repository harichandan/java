package hari.source;

public class IsPalindrome {
  public static void main(String[] args) {
    String str1 = "I am ma I";
    String str2 = "This is not this";
    System.out.println("'" + str1 + "' " + "is a palindrome: " + isPalindrome(str1));
    System.out.println("'" + str2 + "' " + "is a palindrome: " + isPalindrome(str2));
  }

  public static boolean isPalindrome(String s) {
    int i = 0, j = s.length() - 1;
    while (i<j) {
      if(!Character.isLetterOrDigit(s.charAt(i)))
        i++;
      if(!Character.isLetterOrDigit(s.charAt(j)))
        j--;
      
      if(Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j)))
        return false;
      
      i++;
      j--;
    }
    return true;
  }
}
