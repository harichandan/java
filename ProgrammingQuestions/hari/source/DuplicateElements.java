package hari.source;

public class DuplicateElements {
	public boolean duplicateExist(int[] arr) {
		boolean[] bool = new boolean[arr.length];
		for (int i : arr) {
			if (!bool[i]) {
				bool[i] = true;
			} else
				return true;
		}
		return false;
	}

	/*
	 * Method to remove duplicate elements from the sorted array and return
	 * length of new array
	 */
	public int removeDuplicateSortedArray(int[] arr) {
		int index = 0;

		for (int i = 1; i < arr.length; i++)
			if (arr[i] != arr[i - 1])
				arr[++index] = arr[i];

		return index + 1;

	}

	public static void main(String[] args) {
		int arr[] = { 1, 2, 0, 0 };
		int sortedArray[] = { 1, 1, 2, 2, 2, 2, 3, 4, 4, 4 };
		System.out.println(new DuplicateElements().duplicateExist(arr));
		System.out.println(new DuplicateElements().removeDuplicateSortedArray(sortedArray));
	}

}
