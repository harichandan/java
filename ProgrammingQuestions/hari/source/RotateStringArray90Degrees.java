package hari.source;

public class RotateStringArray90Degrees {

  public String[] rotate90(String[] words) {
    int m = words.length;
    int n = words[0].length();
    String[] result = new String[n];
    StringBuilder[] strs = new StringBuilder[n];

    for (int i = 0; i < n; i++) {
      strs[i] = new StringBuilder();
      for (int j = 0; j < m; j++) {
        strs[i].append(words[j].charAt(i));
      }
      result[n - i - 1] = strs[i].toString();
    }

    return result;
  }

  public static void main(String[] args) {
    String[] words = { "apple", "anger", "monks", "stink" };
    String[] rotatedStrings = new RotateStringArray90Degrees().rotate90(words);
    for (int i = 0; i < rotatedStrings.length; i++) {
      System.out.println(rotatedStrings[i]);
    }
  }
}
