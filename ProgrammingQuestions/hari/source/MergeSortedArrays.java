package hari.source;

public class MergeSortedArrays {
  public static void main(String[] args) {
    int[] A = new int[10];
    A[0] = 5;
    A[1] = 6;
    A[2] = 10;
    int[] B = new int[] { 4, 7, 9 };
    A = mergeSortedArrays(A, 3, B, 3);
    for (int x = 0; x < 6; x++) {
      System.out.print(A[x] + " ");
    }

  }

  public static int[] mergeSortedArrays(int[] A, int lastA, int[] B,
      int lastB) {
    int i = lastA - 1;
    int j = lastB - 1;
    int k = lastA + lastB - 1;
    while (i >= 0 && j >= 0)
      A[k--] = (A[i] > B[j]) ? A[i--] : B[j--];

    while (j >= 0)
      A[k--] = B[j--];

    return A;
  }
}
