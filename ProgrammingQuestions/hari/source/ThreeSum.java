package hari.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {
	/*
	 * Find all a,b,c such that a + b + c = 0
	 */
	public List<List<Integer>> threeSum(int[] array) {
		List<List<Integer>> tripletList = new ArrayList<>();
		int l = array.length;
		Arrays.sort(array);
		for (int i = 0; i < l - 2; i++) {
			if (i == 0 || array[i] > array[i - 1]) {
				int j = i + 1;
				int k = l - 1;
				while (j < k) {
					int sum = array[i] + array[j] + array[k];
					if (sum == 0) {
						tripletList.add(Arrays.asList(array[i], array[j], array[k]));
						j++;
						k--;
						while (j < k && array[j] == array[j - 1])
							j++;
						while (j < k && array[k] == array[k + 1])
							k--;
					} else if (sum < 0)
						j++;
					else
						k--;
				}
			}
		}

		return tripletList;
	}

	public static void main(String[] args) {
		System.out.println(new ThreeSum().threeSum(new int[] { -1, 0, 1, 2, -1, -4 }));
	}

}
