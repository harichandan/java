package hari.source;

public class PrimeFactorization {
	public void primeFactors(int n) {
		int temp = n;
		for (int i = 2; i <= Math.sqrt(n); i++) {
			while (temp % i == 0) {
				System.out.print(i + " ");
				temp /= i;
			}
		}

		if (temp > 1)
			System.out.print(temp);
	}

	public static void main(String[] args) {
		new PrimeFactorization().primeFactors(449);
	}

}
