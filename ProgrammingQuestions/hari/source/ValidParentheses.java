package hari.source;

/*
 * Leetcode 20
 * ===========
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', 
 * determine if the input string is valid. The brackets must close in the correct 
 * order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.
 */

public class ValidParentheses {
  public boolean isValid(String s) {
    int count1 = 0, count2 = 0, count3 = 0;
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      switch (c) {
      case '(':
        count1++;
        break;
      case '{':
        count2++;
        break;
      case '[':
        count3++;
        break;
      case ')':
        if (count1 == 0 || count2 > 0 || count3 > 0)
          return false;
        else
          count1--;
        break;
      case '}':
        if (count2 == 0 || count1 > 0 || count3 > 0)
          return false;
        else
          count2--;
        break;
      case ']':
        if (count3 == 0 || count1 > 0 || count2 > 0)
          return false;
        else
          count3--;
        break;
      }
    }
    return (count1 == 0) && (count2 == 0) && (count3 == 0);
  }

  public static void main(String[] args) {
    System.out.println(new ValidParentheses().isValid("()"));
    System.out.println(new ValidParentheses().isValid("([)]"));
    System.out.println(new ValidParentheses().isValid("()[]{}"));
  }

}
