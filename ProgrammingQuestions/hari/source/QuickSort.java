package hari.source;

public class QuickSort {
	private int partition(Integer[] array, int low, int high) {
		int q = array[high];
		int i = low - 1;
		int temp;
		for (int j = low; j < high; j++) {
			if (array[j] < q) {
				i++;
				temp = array[j];
				array[j] = array[i];
				array[i] = temp;
			}
		}
		i++;
		temp = array[i];
		array[i] = array[high];
		array[high] = temp;

		return i;
	}

	private void quicksortHelper(Integer[] array, int low, int high) {
		int q;
		if (low < high) {
			q = partition(array, low, high);
			quicksortHelper(array, low, q - 1);
			quicksortHelper(array, q + 1, high);
		}
	}

	public void quicksort(Integer[] array) {
		quicksortHelper(array, 0, array.length - 1);
	}

	public static void main(String[] args) {
		QuickSort q = new QuickSort();
		Integer[] array = new Integer[] { 5, 3, 4, 1, 2, 9, 7, 6, 8 };
		q.quicksort(array);
		for (int elem : array)
			System.out.println(elem);
	}
}
