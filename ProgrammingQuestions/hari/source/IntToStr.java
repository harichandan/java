package hari.source;

public class IntToStr {
	public static void main(String[] args) {
		int num = -1097;
		System.out.println(intToStr(num));

	}

	public static String intToStr(int num) {
		boolean negative = num < 0;
		if (num < 0)
			num = -num;
		StringBuilder s = new StringBuilder();
		while (num != 0) {
			s.append(num % 10);
			num /= 10;
		}
		if (negative)
			s.append('-');
		return s.reverse().toString();
	}
}
