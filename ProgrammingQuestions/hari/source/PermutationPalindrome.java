package hari.source;

// Check if a permutation of a string can be a palindrome

// If more than one character occurring odd no. of times, then 
// it can't be a palindrome

public class PermutationPalindrome {
  public int charToInt(char c) {
    return c - 'a';
  }

  public boolean permutationPalindrome(String s) {
    int[] chars = new int[26];
    int oddCount = 0;
    for (int i = 0; i < s.length(); i++)
      chars[charToInt(s.charAt(i))]++;

    for (int i = 0; i < 26; i++) {
      if (chars[i] % 2 != 0)
        if (oddCount > 1)
          return false;
        else 
          oddCount++;
    }

    return true;
  }

  public static void main(String[] args) {
    PermutationPalindrome p = new PermutationPalindrome();
    System.out.println("civic: " + p.permutationPalindrome("civic"));
    System.out.println("civil: " + p.permutationPalindrome("civil"));
    System.out.println("iccvv: " + p.permutationPalindrome("iccvv"));
    System.out.println("ivc: " + p.permutationPalindrome("ivc"));
  }
}
