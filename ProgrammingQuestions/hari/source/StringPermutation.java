package hari.source;

public class StringPermutation {

  public static void main(String[] args) {
    String str = "abcd";
    //perm(str, "");
    perm_subset(str, "");
  }

  public static void perm(String str, String rem) {
    if (str.equals("")) {
      System.out.println(rem);
      return;
    }

    for (int i = 0; i < str.length(); i++) {
      String remaining = rem + str.charAt(i);
      String next = str.substring(0, i) + str.substring(i + 1, str.length());
      perm(next, remaining);
    }
  }
  
  public static void perm_subset(String str, String rem) {
    if(str.equals("")) {
      System.out.println(rem);
      return;
    }
    
    perm_subset(str.substring(1), rem+str.charAt(0));
    perm_subset(str.substring(1), rem);
  }

}
