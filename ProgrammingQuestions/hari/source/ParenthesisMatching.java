package hari.source;

// Given a string containing parentheses, and the position of 
// open paren, return the position of the close paren for that 
// open paren. Assume that parens are balanced

public class ParenthesisMatching {
	public int parenMatching(String str, int pos) {
		if (str.charAt(pos) != '(')
			return -1;

		int count = 0, openParenCount = -1;
		for (int i = 0; i < str.length(); i++) {
			if (i == pos)
				openParenCount = count + 1;
			if (str.charAt(i) == '(')
				count++;
			else if (str.charAt(i) == ')') {
				if (count == 0)
					return -1;
				if (count == openParenCount)
					return i;
				count--;
			}

		}
		return -1;
	}

	public static void main(String[] args) {
		ParenthesisMatching paren = new ParenthesisMatching();
		System.out.println("This is () string () of ( balanced()) parens, 24: "
				+ paren.parenMatching("This is () string () of ( balanced()) parens: ", 24));
		System.out.println("((())()), 1: " + paren.parenMatching("((())())", 1));
	}

}