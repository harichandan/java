package hari.source;

public class MissingNumber {
	public int missingNumber(int[] A) {
	  int sum = 0, l = A.length + 1;
	  
	  for (int item:A)
	    sum += item;
	    
	  return (l*(l+1)/2) - sum;
	}
}