package hari.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListPermutations {
	public List<List<Integer>> permutations(List<Integer> list) {
		List<List<Integer>> permutations = new ArrayList<>();
		permutations(0, list, permutations);
		return permutations;
	}

	public void permutations(int i, List<Integer> list, List<List<Integer>> permutations) {
		if (i == list.size() - 1) {
			permutations.add(new ArrayList<>(list));
			return;
		}

		for (int j = i; j < list.size(); j++) {
			Collections.swap(list, i, j);
			permutations(i + 1, list, permutations);
			Collections.swap(list, i, j);
		}

	}

	public static void main(String[] args) {
		Integer[] array = new Integer[] { 1, 2, 3 };
		System.out.println(new ListPermutations().permutations(Arrays.asList(array)));

	}

}
