package hari.source;

public class IsNumPalindrome {
  // O(n) time complexity. Takes n operations (where n=no.of digits)
  public boolean isPalindromeReverse(int n) {
    return reverse(n) == n;
  }

  public int reverse(int n) {
    int rev = 0;
    while (n > 0) {
      rev = rev * 10 + n % 10;
      n /= 10;
    }
    return rev;
  }

  // O(n) time complexity. Takes n/2 operations
  public boolean isPalindrome(int n) {
    int numDigits = 0;
    int temp = n;
    int secondHalf = 0;

    while (temp > 0) {
      temp /= 10;
      numDigits++;
    }

    temp = numDigits;
    while (temp > Math.ceil((float) numDigits / 2)) {
      temp--;
      secondHalf += (n % 10)
          * (int) (Math.pow(10, temp - Math.ceil((float) numDigits / 2)));
      n /= 10;
    }
    if (numDigits % 2 == 1)
      n /= 10;

    return n == secondHalf;
  }

  public static void main(String[] args) {
    IsNumPalindrome pal = new IsNumPalindrome();
    System.out.println(pal.isPalindrome(1010101));
    System.out.println(pal.isPalindromeReverse(1010101));
  }
}
