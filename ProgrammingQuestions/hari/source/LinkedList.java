package hari.source;

public class LinkedList {
	class Node {
		int data;
		Node next;

		Node(int data) {
			this.data = data;
		}
	}

	private Node head;

	LinkedList() {
		head = null;
	}

	public Node getHead() {
		return head;
	}

	/*
	 * Add node to the head of the Linked List
	 */
	public void addNode(int data) {
		if (head == null) {
			head = new Node(data);
			return;
		}
		Node node = new Node(data);
		node.next = head;
		head = node;
	}

	/*
	 * Print the linked list
	 */
	public void printLinkedList(Node head) {
		Node node = head;
		while (node != null) {
			System.out.print(node.data + " ");
			node = node.next;
		}
		System.out.println();
	}

	/*
	 * Reverse Linked List
	 */
	public void reverseLinkedList(Node head) {
		Node prev = null;
		Node curr = head;
		Node next;

		while (curr != null) {
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}

		this.head = prev;
	}

	/*
	 * Find middle of the linked list
	 */
	public Node middleLL(Node head) {
		Node slow = head, fast = head;

		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}

		return slow;
	}

	/*
	 * Find node in n'th percent of the list Input constraint: 0.0 <= n <= 1.0
	 * If n = 0.0, return head If n = 1.0, return tail
	 */

	public void nthpercentNode(double percent) {
		if (head.next == null) {
			// Print head if there's only one node in the list
			System.out.println(head.data);
			return;
		}
		double count = 0;
		Node slow = head, fast = head;

		while (fast != null) {
			fast = fast.next;
			count += percent;
			if (count >= 1.0) {
				slow = slow.next;
				count -= 1.0;
			}
		}

		System.out.println("Node in " + percent * 100 + " is: " + slow.data);
	}

	public void removeNthNodeFromEnd(int n) {
		Node dummyHead = new Node(-1);
		dummyHead.next = head;
		Node first = head, second = dummyHead;
		while (n > 0) {
			n--;
			first = first.next;
		}

		while (first != null) {
			first = first.next;
			second = second.next;
		}
		second.next = second.next.next;
	}

	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		list.addNode(0);
		list.addNode(3);
		list.addNode(2);
		list.addNode(1);
		list.addNode(5);
		list.printLinkedList(list.getHead());

		list.reverseLinkedList(list.getHead());
		list.printLinkedList(list.getHead());

		list.removeNthNodeFromEnd(2);
		list.printLinkedList(list.getHead());

	}
}
