package hari.source;

import java.util.HashSet;
import java.util.Set;

public class SudokuValidator {
    /*
     * Iterates over all the boxes Boxes with starting rows and starting columns
     * are: 
     *      (0,0), (0,3), (0,6) 
     *      (3,0), (3,3) ,(3,6) 
     *      (6,0), (6,3), (6,6)
     * 
     * Returns: 
     *      false: if a box is invalid 
     *      true : if all boxes are valid
     */
    private boolean boxesValid(char[][] board) {
        for (int i : new int[] { 0, 3, 6 })
            for (int j : new int[] { 0, 3, 6 })
                if (!boxValidHelper(board, i, j))
                    return false;
        return true;
    }

    /*
     * Checks all the cells in a given box 
     * 
     * Returns: 
     *      false: if the box contains a repeated element (other than '.') 
     *      true : if the box doesn't contain any repeated element
     */
    private boolean boxValidHelper(char[][] board, int startRow, int startCol) {
        Set<Character> set = new HashSet<>();
        for (int i = startRow; i < startRow + 3; i++) {
            for (int j = startCol; j < startCol + 3; j++) {
                if (board[i][j] != '.') {
                    /*
                     * If the set already contains the element, it means that this element is
                     * repeated in this box, and the box is invalid
                     */
                    if (set.contains(board[i][j]))
                        return false;
                    // Add the element to the set if it's not present in the set
                    set.add(board[i][j]);
                }
            }
        }
        return true;
    }

    /*
     * Iterates over all rows
     * 
     * Returns: 
     *      false: if a row is invalid 
     *      true : if all rows are valid
     *
     */
    private boolean rowsValid(char[][] board) {
        for (int i = 0; i < 9; i++)
            if (!rowValidHelper(board, i))
                return false;
        return true;
    }

    /*
     * Checks all the cells in a given row 
     * Returns: 
     *      false: if the row contains a repeated element (other than '.') 
     *      true : if the row doesn't contain any repeated element
     */
    private boolean rowValidHelper(char[][] board, int row) {
        Set<Character> set = new HashSet<>();
        for (int j = 0; j < 9; j++) {
            if (board[row][j] != '.') {
                /*
                 * If the set already contains the element, it means that this element is
                 * repeated in this row, and the row is invalid
                 */
                if (set.contains(board[row][j]))
                    return false;
                // Add the element to the set if it's not present in the set
                set.add(board[row][j]);
            }
        }
        return true;
    }

    /*
     * Iterates over all columns
     * 
     * Returns: 
     *      false: if a column is invalid 
     *      true : if all columns are valid
     *
     */
    private boolean columnsValid(char[][] board) {
        for (int j = 0; j < 9; j++)
            if (!columnValidHelper(board, j))
                return false;
        return true;
    }

    /*
     * Checks all the cells in a given column 
     * Returns: 
     *      false: if the column contains a repeated element (other than '.') 
     *      true : if the column doesn't contain any repeated element
     */
    private boolean columnValidHelper(char[][] board, int column) {
        Set<Character> set = new HashSet<>();
        for (int i = 0; i < 9; i++) {
            if (board[i][column] != '.') {
                /*
                 * If the set already contains the element, it means that this element is
                 * repeated in this column, and the column is invalid
                 */
                if (set.contains(board[i][column]))
                    return false;
                // Add the element to the set if it's not present in the set
                set.add(board[i][column]);
            }
        }
        return true;
    }

    // Returns true if all boxes, all rows and all columns are valid
    public boolean isSudokuValid(char[][] board) {
        return boxesValid(board) && rowsValid(board) && columnsValid(board);
    }

    public static void main(String[] args) {
        char[][] validBoard = { 
                { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
                { '6', '.', '.', '1', '9', '5', '.', '.', '.' }, 
                { '.', '9', '8', '.', '.', '.', '.', '6', '.' },
                { '8', '.', '.', '.', '6', '.', '.', '.', '3' }, 
                { '4', '.', '.', '8', '.', '3', '.', '.', '1' },
                { '7', '.', '.', '.', '2', '.', '.', '.', '6' }, 
                { '.', '6', '.', '.', '.', '.', '2', '8', '.' },
                { '.', '.', '.', '4', '1', '9', '.', '.', '5' }, 
                { '.', '.', '.', '.', '8', '.', '.', '7', '9' } };
        System.out.println("Sudoku is valid: " + new SudokuValidator().isSudokuValid(validBoard));

        char[][] invalidBoard = { 
                { '5', '3', '.', '.', '7', '.', '.', '.', '.' },
                { '5', '.', '.', '1', '9', '5', '.', '.', '.' }, 
                { '.', '9', '8', '.', '.', '.', '.', '6', '.' },
                { '8', '.', '.', '.', '6', '.', '.', '.', '3' }, 
                { '4', '.', '.', '8', '.', '3', '.', '.', '1' },
                { '7', '.', '.', '.', '2', '.', '.', '.', '6' }, 
                { '.', '6', '.', '.', '.', '.', '2', '8', '.' },
                { '.', '.', '.', '4', '1', '9', '.', '.', '5' }, 
                { '.', '.', '.', '.', '8', '.', '.', '7', '9' } };
        System.out.println("Sudoku is valid: " + new SudokuValidator().isSudokuValid(invalidBoard));

    }
}
