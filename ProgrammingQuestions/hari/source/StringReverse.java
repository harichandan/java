package hari.source;

public class StringReverse {

  public String reverse(String str) {

    StringBuilder rev = new StringBuilder(str.length());

    for (int i = str.length() - 1; i >= 0; i--)
      rev.append(str.charAt(i));

    return rev.toString();

  }
}