package hari.source;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* 
 * Given a string and a number n, return all characters
 * that appear at least n times
 */

public class RepeatedCharsInString {
  private int increment(Integer accumulator) {
    if (accumulator == null)
      return 1;
    return accumulator + 1;
  }

  public List<Character> repeatedChars(String s, int n) {
    Map<Character, Integer> map = new HashMap<>();
    List<Character> charList = new ArrayList<>();
    for (int i = 0; i < s.length(); i++) {
      char ch = s.charAt(i);
      if (Character.isAlphabetic(ch))
        map.put(ch, increment(map.get(ch)));
    }

    for(Map.Entry<Character, Integer> entry : map.entrySet()) {
      if (entry.getValue() >= n)
        charList.add(entry.getKey());
    }
    
    return charList;
  }

  public static void main(String[] args) {
    RepeatedCharsInString r = new RepeatedCharsInString();
    System.out.println(r.repeatedChars("i am the best person in the world", 3));
  }
}