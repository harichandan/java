package hari.source;

public class RevNum {
	public int revnum(int num) {
		boolean sign = num < 0;
		int rev = 0;
		num = sign ? -num : num;
		while (num != 0) {
			rev = rev * 10 + num % 10;
			num /= 10;
		}
		return sign ? -rev : rev;
	}

}
