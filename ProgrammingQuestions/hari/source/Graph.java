package hari.source;

import java.util.ArrayList;
import java.util.List;

public class Graph {
	private int V;
	private List<List<Integer>> graph;

	public int totalVertices() {
		return V;
	}

	public void addEdge(int u, int v) {
		graph.get(u).add(v);
	}

	public void addVertex(int u) {
		graph.add(V, new ArrayList<Integer>());
		V++;
	}

	public void printEdges() {
		for (int i = 0; i < V; i++) {
			List<Integer> edgeList = graph.get(i);
			for (int j = 0; j < edgeList.size(); j++)
				System.out.println("(" + i + ", " + edgeList.get(j) + ")");
		}
	}

	public List<Integer> getNeighbors(int u) {
		return graph.get(u);
	}

	Graph(int V) {
		this.V = V;
		graph = new ArrayList<>();
		for (int i = 0; i < V; i++) {
			graph.add(i, new ArrayList<Integer>());
		}
	}

	public static void main(String[] args) {
		Graph graph = new Graph(4);
		graph.addEdge(0, 1);
		graph.addEdge(1, 2);
		graph.addEdge(2, 3);
		graph.addEdge(3, 1);
		graph.addVertex(4);
		graph.addEdge(3, 4);
		graph.printEdges();
	}
}
