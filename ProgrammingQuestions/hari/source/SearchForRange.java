package hari.source;

import java.util.ArrayList;
import java.util.List;
/*
 * Leetcode 34
 * -----------
 * 
 * Given an array of integers sorted in ascending order, find the 
 * starting and ending position of a given target value. Your 
 * algorithm's runtime complexity must be in the order of O(log n).
 * 
 * If the target is not found in the array, return [-1, -1].
 * For example,
 * 		Given [5, 7, 7, 8, 8, 10] and target value 8, return [3, 4].
 */

public class SearchForRange {
	private int binarySearch(int[] array, int target, boolean lowIndex) {
		int len = array.length;
		int low = 0, high = len, mid;
		while (low <= high) {
			mid = low + ((high - low) / 2);
			if (array[mid] == target) {
				if (lowIndex) {
					if (mid == 0 || (mid - 1 >= 0 && array[mid - 1] < target))
						return mid;
					else
						high = mid - 1;
				} else {
					if ((mid == len - 1) || (mid + 1 < len && array[mid + 1] > target))
						return mid;
					else
						low = mid + 1;
				}
			} else if (array[mid] < target)
				low = mid + 1;
			else
				high = mid - 1;
		}
		return -1;
	}

	public List<Integer> searchRange(int[] array, int target) {
		List<Integer> result = new ArrayList<>();
		result.add(binarySearch(array, target, true));
		result.add(binarySearch(array, target, false));
		return result;
	}

	public static void main(String[] args) {
		int[] array = { 5, 7, 7, 8, 8, 10 };
		System.out.println(new SearchForRange().searchRange(array, 6));
	}
}
