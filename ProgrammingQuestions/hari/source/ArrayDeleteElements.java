package hari.source;

public class ArrayDeleteElements {
	public static void main(String[] args) {
		int[] a = new int[] { 1, 2, 3, 2, 5, 2, 6, 2, 7 };
		System.out.println(array_element_delete(a, 2));

		int[] b = new int[] { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
		System.out.println(array_duplicate_sorted(b));
	}

	public static int array_element_delete(int[] a, int key) {
		int i, count = 0;
		for (i = 0; i < a.length; i++) {
			if (a[i] != key) {
				a[count++] = a[i];
			}
		}
		return count;
	}

	public static int array_duplicate_sorted(int[] a) {
		int i, count = 1;
		for (i = 1; i < a.length; i++) {
			if (a[i] != a[i - 1]) {
				a[count++] = a[i];
			}
		}
		return count;
	}
}
